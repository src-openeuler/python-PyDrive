Name:           PyDrive2
Version:        1.21.1
Release:        1
Summary:        Google Drive API Python wrapper library, maintained fork of PyDrive

License:        Apache-2.0
URL:            https://github.com/iterative/PyDrive2
Source0:        https://files.pythonhosted.org/packages/source/p/pydrive2/pydrive2-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  openEuler-rpm-config

Requires:       python3-google-api-client python3-pyOpenSSL

%description
Google Drive API Python wrapper library. Maintained fork of PyDrive.

%package -n     python3-%{name}
Summary:        %{summary}

Obsoletes:      python3-PyDrive < 1.18.0

%description -n python3-%{name}
Google Drive API Python wrapper library. Maintained fork of PyDrive.

%prep
%autosetup

%generate_buildrequires

%build
%py3_build

%install
%py3_install

# No check as requires credentials for GoogleAuth

%files -n python3-%{name}
%doc README.rst
%license LICENSE
%{python3_sitelib}

%changelog
* Tue Nov 05 2024 songliyang <songliyang@kylinos.cn> - 1.21.1-1
- Upgrade to version 1.21.1
- rename get file args to fsspec spec
- installation to get fsspec working
- fix spelling

* Mon Jul 15 2024 zhaoshuang <zhaoshuang@uniontech.com> - 1.20.0-1
- Upgrade to version 1.20.0
  fix(fs): add dirs caching, make it a bit more robust
  feat(auth): add GDRIVE_NON_INTERACTIVE to avoid launching browser
  chore(fs): add tests to cover recent PRs
  Partial compatibility with Google security update 2021

* Tue Dec 12 2023 peijiankang <peijiankang@kylinos.cn> - 1.18.0-1
- Init 1.18.0
